import logging

import responses
from flask.testing import FlaskClient
from flask_fillin import FormWrapper
from freezegun import freeze_time
from lettuce import before, after, world

from shipka3.store import database
from shipka3.webapp import create_app, init_app
from shipka3.webapp.util import get_now_utc

# Setup context
world.ctx = None
world.client = None


class PutAnythingHere(object):
    """
    Used to create a local variable in world
    """

    def __getattr__(self, name):
        try:
            return object.__getattribute__(self, name)
        except AttributeError:
            return None


@before.all
def setup_app():
    # need to freeze time so that time specific functional tests are not flaky
    world.freezer = freeze_time("2015-03-14 09:26:53")
    world.freezer.start()
    world.now = get_now_utc()
    # start responses mock
    world.responses_mock = responses.RequestsMock()
    world.responses_mock.start()
    # create a test client app
    app = create_app()
    world.app = init_app(app, 'test')
    world.app.logger.setLevel(logging.ERROR)


def setup(outline=False):
    if not outline:
        world.client = FlaskClient(world.app, use_cookies=True, response_wrapper=FormWrapper)
        world.ctx = world.app.test_request_context()
        world.ctx.push()
    database.create_db()

    # world.mail_mgr = (mail.record_messages())
    # world.mail_exit = type(world.mail_mgr).__exit__
    # value = type(world.mail_mgr).__enter__(world.mail_mgr)
    # world.mail_safety_outbox = value
    #
    # world.email_sender = world.app.config.get("EMAIL_SENDER")
    # world.email_sender.mailbox = []

    world.local = PutAnythingHere()


def teardown(outline=False):
    database.drop_db()
    if not outline:
        world.ctx.pop()

    # assert_equal(len(world.mail_safety_outbox), 0)
    # world.mail_exit(world.mail_mgr, None, None, None)
    world.step_outbox = 0


@before.each_scenario
def before_scenario(scenario):
    setup()


@before.each_outline
def before_outline(scenario, outline):
    setup(outline=True)


@after.each_scenario
def after_scenario(scenario):
    teardown()


@after.each_outline
def after_outline(scenario, outline):
    teardown(outline=True)


def expect_mail(func):
    """
    Puts a local variable outbox for catching mail

    Use it to decorate the step function which sends an email
    :param func:
    :return:
    """

    def _decorator(self, *args, **kwargs):
        pass
        # with mail.record_messages() as world.step_outbox:
        #     func(self, *args, **kwargs)
        #     world.mail_safety_outbox = [o for o in world.mail_safety_outbox if o not in world.step_outbox]

    return _decorator


@after.all
def teardown_app(total):
    world.freezer.stop()
    world.responses_mock.stop()
    world.responses_mock.reset()

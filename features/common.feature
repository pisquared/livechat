# Created by pi2 at 22/07/16
Feature: General tests

  Scenario: Publicly visible URLs should not return error
    When Visit all publicly visible GET urls

  Scenario: Index page displays correctly
    When I visit the url "/"
    Then I see the response contains "Prod Day"

  Scenario: Login page displays correctly when not logged in
    When I visit the url "/login"
    Then I see the response contains "Email Address"
    And I see the response contains "Password"
    And I see the response contains "Remember Me"
    And I see the response contains "Login"

  Scenario: Registration page displays correctly
    When I visit the url "/register"
    And I see the response contains "Email"
    And I see the response contains "Password"
    And I see the response contains "Retype Password"
    And I see the response contains "Register"

  Scenario Outline: Register wrongly
    When I try to register with <name>, <email>, <password> and <password_confirm>
    Then I see the response contains "<error>"

    Examples:
      | email                 | password | password_confirm | error                                  |
      | shop_owner@opinew.com |          |                  | Password not provided                  |
      | shop_owner@opinew.com | 1234     | 1234             | Password must be at least 6 characters |
      | shop_owner@opinew.com | bogus14  | bogus1234        | Passwords do not match                 |

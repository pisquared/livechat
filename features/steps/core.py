import httplib
import json

from lettuce import step, world
from nose.tools import assert_equals, assert_in, assert_not_in, assert_false, assert_true

from shipka3.store import database


#######################################################################################################################
# API
#######################################################################################################################


class TestAPI(object):
    assert_equals = assert_equals
    assert_true = assert_true
    assert_false = assert_false
    assert_in = assert_in
    assert_not_in = assert_not_in

    @staticmethod
    def string_splitter(attributes):
        return dict([t.split('=') for t in attributes.split(',')])

    @classmethod
    def _login_user(cls, user):
        cls.i_visit_the_url("/login-user-by-id?user_id={user_id}".format(user_id=user.id))

    @staticmethod
    def _follow_redirects(response):
        while response.status_code in range(300, 400):
            response = world.client.get(response.location)
        return response

    @classmethod
    def _handle_response(cls, response):
        world.local.redirect_response = response
        world.local.response = response = cls._follow_redirects(response)
        return response

    # GIVENs
    @staticmethod
    def there_is_a_model(model):
        model_class = getattr(database, model)
        instance = model_class.create()
        database.add(instance)
        database.push()
        return instance

    @staticmethod
    def there_is_a_model_with_attributes(model, **kwargs):
        model_class = getattr(database, model)
        instance = model_class.create(**kwargs)
        database.add(instance)
        database.push()
        return instance

    @staticmethod
    def there_are_x_models_with_attributes(x, model, **kwargs):
        model_class = getattr(database, model)
        instances = list()
        for _ in range(x):
            instances.append(model_class.create(**kwargs))
        for instance in instances:
            database.add(instance)
        database.push()
        return instances

    @classmethod
    def i_view_pages_as_a_user(cls, user_role):
        import assets
        user = None
        if user_role == "PUBLIC":
            response = cls.i_visit_the_url("/get-real-or-anonymous-user-id")
            user = database.User.get_one_by(id=int(response.data))
        elif user_role == "ANOTHER_PUBLIC":
            response = cls.i_visit_the_url("/logout-or-renew-anonymous-user-id")
            user = database.User.get_one_by(id=int(response.data))
        elif user_role == "REVIEWER":
            reviewer_role = database.Role.get_one_by(name=assets.Roles.REVIEWER_ROLE)
            user = database.User.create(roles=[reviewer_role])
            database.add(user)
            database.push()
            cls._login_user(user)
        elif user_role == "SHOP_OWNER":
            reviewer_role = database.Role.get_one_by(name=assets.Roles.SHOP_OWNER_ROLE)
            user = database.User.create(email="shop_owner@opinew.com", password="fake", roles=[reviewer_role])
            shop = database.Shop.get_one_by() or database.Shop.create()
            shop.owner = user
            database.add(user)
            database.push()
            world.local.shop = shop
            cls._login_user(user)
        world.local.user = user
        return user

    # WHENs
    @classmethod
    def i_visit_the_url(cls, url, **kwargs):
        response = world.client.get(url, **kwargs)
        return cls._handle_response(response)

    @classmethod
    def i_post_to_url_with_arguments(cls, url, **arguments):
        response = world.client.post(url, data=arguments)
        return cls._handle_response(response)

    @classmethod
    def i_submit_the_form(cls, data=None, referer=None):
        if not data:
            data = dict()
        form = world.local.response.form
        response = form.submit(world.client,
                               data=data,
                               headers={"Referer": referer})
        return cls._handle_response(response)

    # THENs
    @staticmethod
    def i_get_redirected_to(url):
        assert_equals(world.local.redirect_response.status_code, httplib.FOUND)
        assert_equals(world.local.redirect_response.location, url)

    @staticmethod
    def i_see_the_response_contains(expected):
        assert_in(expected, world.local.response.data.decode('utf-8'))

    @staticmethod
    def i_see_the_response_does_not_contain(expected):
        assert_not_in(expected, world.local.response.data.decode('utf-8'))

    @staticmethod
    def i_see_the_response_is_valid_json():
        assert_equals(world.local.response.content_type, "application/json")
        json.loads(world.local.response.data)

    @staticmethod
    def the_database_now_contains_x_instances_of_model_with_attributes(x, model, **kwargs):
        model_class = getattr(database, model)
        if not model_class:
            raise AttributeError
        instances = model_class.get_all_by(**kwargs)
        world.local.instances = instances
        assert_equals(len(instances), int(x))
        return instances

    @staticmethod
    def an_email_is_send_to_from_with_subject_containing_message(destination, origin, subject, message):
        assert_equals(len(world.email_sender.mailbox), 1)
        email = world.email_sender.mailbox[0]
        assert_equals(origin, email.origin)
        assert_equals(destination, email.destination)
        assert_equals(subject, email.subject)
        assert_in(message, email.content)

    @staticmethod
    def no_email_is_sent():
        assert_equals(len(world.email_sender.mailbox), 0)


@step('I view pages as a (PUBLIC|ANOTHER_PUBLIC|REVIEWER|SHOP_OWNER) user')
def i_view_pages_as_a_user(step, user_role):
    TestAPI.i_view_pages_as_a_user(user_role)


@step('I get redirected to "([^"]*)"')
def i_get_redirected_to(step, url):
    TestAPI.i_get_redirected_to(url)


@step('I visit the url "([^"]*)"')
def i_visit_the_url(step, url):
    TestAPI.i_visit_the_url(url)


@step('I see the response is valid json')
def i_see_the_response_is_valid_json(step):
    TestAPI.i_see_the_response_is_valid_json()


@step('There is a "([^"]*)"')
def there_is_a_model_with_attributes(step, model):
    TestAPI.there_is_a_model(model)


@step('There is a "([^"]*)" with "([^"]*)"')
def there_is_a_model_with_attributes(step, model, attributes):
    kwargs = TestAPI.string_splitter(attributes)
    TestAPI.there_is_a_model_with_attributes(model, **kwargs)


@step('I POST to "([^"]*)" with "([^"]*)"')
def i_post_to_url_with_arguments(step, model, arguments):
    kwargs = TestAPI.string_splitter(arguments)
    TestAPI.i_post_to_url_with_arguments(model, **kwargs)


@step('I see the response contains "([^"]*)"')
def i_see_the_response_contains(step, expected):
    TestAPI.i_see_the_response_contains(expected)


@step('I see the response DOES NOT contain "([^"]*)"')
def i_see_the_response_does_not_contain(step, expected):
    TestAPI.i_see_the_response_does_not_contain(expected)


@step('The database now contains (\d+) instances of "([^"]*)" with "([^"]*)"')
def the_database_now_contains_model_with_attributes(step, x, model, attributes):
    kwargs = TestAPI.string_splitter(attributes)
    TestAPI.the_database_now_contains_x_instances_of_model_with_attributes(x, model, **kwargs)


@step('An email is sent to "([^"]*)" from "([^"]*)" with subject "([^"]*)" containing "([^"]*)"')
def an_email_is_send_to_from_with_subject_containing_message(step, destination, origin, subject, message):
    TestAPI.an_email_is_send_to_from_with_subject_containing_message(destination, origin, subject, message)


@step('No email is sent')
def no_email_is_sent(step):
    TestAPI.no_email_is_sent()

# -*- coding: utf-8 -*-

from flask import url_for
from lettuce import step, world
from werkzeug.routing import BuildError

from core import TestAPI


@step('Visit all publicly visible GET urls')
def test_public_urls(step):
    """
    Tests all publicly reachable GET routes
    """
    for rule in world.app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods:
            if rule.endpoint in ['static', 'user.reset_password', 'user.confirm_email'] or \
                    rule.endpoint.startswith('dev_test'):
                continue
            try:
                url = url_for(rule.endpoint, **rule.defaults or {})
                world.client.get(url, follow_redirects=True)
            except (KeyError, BuildError):
                # skip the ones that don't define defaults - otherwise a KeyError is raised
                continue


@step("I try to register with (?P<name>.*), (?P<email>.*), (?P<password>.*) and (?P<password_confirm>.*)")
def step_impl(step, name, email, password, password_confirm):
    TestAPI.i_post_to_url_with_arguments('/register',
                                         name=name,
                                         email=email,
                                         password=password,
                                         password_confirm=password_confirm)

# LiveChat

*Chat by seeing other's messages while they type it*

## Setup

To start working on this project, make sure you have git installed, clone the repo and bootstrap the project:

```
git clone <project_url>
cd livechat2
./manage.py bootstrap
```

This will clone [shipka3](https://gitlab.com/pisquared/shipka3) repo and do some more initial setup.

Install [vagrant](https://www.vagrantup.com/downloads.html) and [virtualbox](https://www.virtualbox.org/wiki/Downloads) for your OS and then to bring up the virtual machine run:

```
vagrant up
```


After the machine complete provisioning, you can ssh with:

```
vagrant ssh
```

## Check status

Once inside the virtual environment, you can run the `status` command to see the running services:

```
$ ./manage.sh status

monit            Monitoring                 enabled    running    accessible    200  http://localhost:2812
nginx            Reverse proxy              enabled    running    accessible    200  http://localhost:80
eventlet         Local server               enabled    running    accessible    200  http://localhost:5000
postgresql       Database                   enabled    running
pgweb            Database management        enabled    running    accessible    200  http://localhost:8081
celery           Workers                    enabled    running
beat             Scheduling                 enabled    running
flower           Workers management         enabled    running    accessible    200  http://localhost:5555
rabbitmq-server  Queue management           enabled    running    accessible    200  http://localhost:15672
                 Webapp > Queue > Wrk       sent       completed
```

## Test

To execute the tests, run:

```
$ ./manage.sh test
```

## Run dev

To run development server:

```
$ ./manage.sh runserver
```

## Powered by Shipka3

For more commands, explanation of setup etc, checkout `./manage.sh` with no arguments and the [documentation of shipka3](https://gitlab.com/pisquared/shipka3/blob/master/README.md).


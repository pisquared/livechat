from celery.result import AsyncResult

from webapp.client import client


@client.route('/tasks/sum/<int:x>/<int:y>')
def start_async_task(x, y):
    from workers.tasks import sum_args
    task_id = sum_args.delay(x, y)
    return "result will be available here <a href='/tasks/{task_id}/result'>{task_id}</a>".format(task_id=task_id)


@client.route('/tasks/<task_id>/result')
def get_async_task_result(task_id):
    result = AsyncResult(id=task_id)
    return "Result: {result}".format(result=result.get())

from flask import render_template, request, session
from flask_socketio import emit

from shipka3.webapp import socketio
from shipka3.webapp.util import get_user
from webapp.client import client

COMMITED = []
DB = {}


@client.route('/')
def get_home():
    user = get_user()
    return render_template('index.html', user=user)


@socketio.on('disconnect')
def initial_connect():
    print "DISCONNECT", request.sid
    emit('client_disconnect', {'sid': request.sid})
    if request.sid in DB:
        del DB[request.sid]


@socketio.on('connect')
def initial_connect():
    print "CONNECT", request.sid
    if 'name' in session:
        DB[request.sid] = {'name': session['name']}
    emit('srv_connect', {'DB': DB, 'COMMITED': COMMITED})


@socketio.on('sid')
def handle_message(message):
    emit('sid', {'sid': request.sid})


@socketio.on('message_s')
def handle_message(message_envelope):
    print message_envelope
    print request.sid
    DB[request.sid] = message_envelope
    session['name'] = message_envelope['name']
    print DB
    emit('message_r', {'m': message_envelope, 'sid': request.sid}, broadcast=True)


@socketio.on('message_commit')
def handle_message_commit(message_envelope):
    COMMITED.append(message_envelope)
    print '>>> commit'
    print message_envelope
    emit('message_commit', message_envelope, broadcast=True)

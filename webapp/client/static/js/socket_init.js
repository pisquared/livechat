var webSocketsAvailable = false;
if ("WebSocket" in window) {
  socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);
  socket.on('connect', function () {
    var payload = {
      tz_offset_minutes: new Date().getTimezoneOffset(),
      address: location.pathname
    };
    if (DEBUG)
      console.log('> SYN: ' + JSON.stringify(payload));
    socket.emit('SYN', payload);
  });
  socket.on('ACK', function (response) {
    if (DEBUG)
      console.log('< ACK: ' + JSON.stringify(response));
    // acquired response, mark sockets available
    webSocketsAvailable = true;
  });
}
var oldSync = Backbone.ajaxSync;
Backbone.ajaxSync = function (method, model, options) {
  options.beforeSend = function (xhr) {
    xhr.setRequestHeader('X-CSRFToken', CSRF_TOKEN);
  };
  return oldSync(method, model, options);
};

var NOTIFIER = {};
var REQUEST_SID = "not_set";
_.extend(NOTIFIER, Backbone.Events);

var ApplicationRegistry = (function () {
  /**
   * Register your applications here
   * @type {{}}
   */
  var apps = {};
  var socket;

  /**
   * Initialize the application registry with websockets
   */
  var init = function () {
    var webSocketsAvailable = false;
    if ("WebSocket" in window) {
      socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);
      socket.on('connect', function () {
        var payload = {tz_offset_minutes: new Date().getTimezoneOffset(), address: location.pathname};
        if (DEBUG)
          console.log('> components_syn: ' + JSON.stringify(payload));
        socket.emit('components_syn', payload);
      });
      socket.on('components_ack', function (response) {
        if (DEBUG)
          console.log('< components_ack: ' + JSON.stringify(response));
        // acquired response, mark sockets available
        webSocketsAvailable = true;
        if ($.isEmptyObject(response.length)) {
          REQUEST_SID = response['request_sid'];
          updateFromServer(response);
        }
      });
    }
  };

  /**
   * What is the latest time that this appName has been updated
   * @param appName
   */
  var _getLatestLocalUpdate = function (appName) {
    var localStorageName = appName + "-collection-retrieved";
    return localStorage.getItem(localStorageName);
  };

  /**
   * Update the latest time an appName has been updated
   * @param appName
   */
  var _updateLatestUpdate = function (appName) {
    var localStorageName = appName + "-collection-retrieved";
    localStorage.setItem(localStorageName, Math.floor(Date.now() / 1000));
  };

  /**
   * Callback from a socket to update apps from init socket
   * @param socketResponse
   */
  var updateFromServer = function (socketResponse) {
    $.each(apps, function (appName, app) {
      var modelUpdate = socketResponse.models_updates[appName];
      if (!modelUpdate)
        return;
      var latestFromServer = modelUpdate.latest;
      var latestLocal = _getLatestLocalUpdate(appName);
      if (!latestLocal || latestFromServer > latestLocal) {
        // the server has been updated later than current date
        app.collection.fetch({
          ajaxSync: true,
          success: function () {
            _updateLatestUpdate(appName);
            app.collection.each(function (model) {
              model.save();
            });
          }
        });
      }
    });
  };


  /**
   * Register created, updated, deleted socket events
   * @param appName
   */
  var registerSocketEvents = function (appName) {
    $.each(['created', 'updated', 'deleted'], function (_, eventName) {
      socket.on(appName + '.' + eventName, function (response) {
        var request_sid = response['request_sid'];
        if (DEBUG)
          console.log(appName + '_' + eventName, 'fromMe?:', request_sid === REQUEST_SID, response);
        if (request_sid !== REQUEST_SID)
          NOTIFIER.trigger("server_" + eventName + ":" + appName, response);
      });
    })
  };

  // ******************************************************************
  // initialize the registry
  init();

  // ******************************************************************
  // public interfaces
  return {
    /**
     * Register an app with a name
     * @param name
     * @param app
     */
    register: function (name, app) {
      apps[name] = app;
      app.collection.fetch();
      registerSocketEvents(name);
    },

    /**
     * What is the latest time that this appName has been updated
     * @param appName
     */
    getLatestLocalUpdate: function (appName) {
      _getLatestLocalUpdate(appName);
    },

    /**
     * Update the latest time an appName has been updated
     * @param appName
     */
    updateLatestUpdate: function (appName) {
      _updateLatestUpdate(appName);
    },

    /**
     * Fetch all periods for an app
     * @param start
     * @param end
     */
    getPeriod: function (start, end) {
      var data = {};
      if (start)
        data['start_ts'] = start;
      if (end)
        data['end_ts'] = end;
      $.each(apps, function (appName, app) {
        if (app.views) {
          $.each(app.views, function (_, view) {
            if (view.updatePeriod) {
              view.updatePeriod(start, end);
            }
          })
        }
        app.collection.fetch({
          ajaxSync: true,
          data: data
        })
      });
    }
  }
})();

var AppListView = Backbone.View.extend({
  initialize: function (options) {
    this.listenTo(this.collection, 'sync change update', this.render);
    this.listenTo(NOTIFIER, 'server_created:' + this.appName, this.serverCreated);
    this.listenTo(NOTIFIER, 'server_updated:' + this.appName, this.serverUpdated);
    this.listenTo(NOTIFIER, 'server_deleted:' + this.appName, this.serverDeleted);
    if (options)
      this.updatePeriod = options.updatePeriod || function (start, end) {
      };
  },

  serverCreated: function (response) {
    this.collection.add(response['instance'], {at: 0});
    ApplicationRegistry.updateLatestUpdate(this.appName);
  },

  serverUpdated: function (response) {
    var _model = this.collection.findWhere({id: response.id});
    if (_model) {
      _model.set(response.updates);
      ApplicationRegistry.updateLatestUpdate(this.appName);
    }
  },

  serverDeleted: function (response) {
    this.collection.remove(response.id);
    ApplicationRegistry.updateLatestUpdate(this.appName);
  }
});

var AppElementView = Backbone.View.extend({
  events: function () {
    return {
      'click .action-edit': 'clickEdit'
    }
  },

  editView: null,

  clickEdit: function () {
    if (!this.editView)
      this.editView = new this.EditViewClass({model: this.model});
    else
      this.editView.model = this.model;
    this.editView.render();
  },

  initialize: function () {
    this.render();
  },

  render: function () {
    var html = this.template(this.model.toJSON());
    this.setElement(html);
    return this;
  }
});

var AppEditView = Backbone.View.extend({
  el: '#modal',

  events: {
    'click .btn-submit': 'onEdit',
    'click .action-delete': 'onDelete'
  },

  render: function () {
    var html = this.template(this.model.toJSON());
    this.$('.modal-content').html(html);
    // need to redelegate events as it's a shared modal for both create and edit
    this.$el.off();
    this.delegateEvents();
    return this;
  },

  onEdit: function () {
    var attrs = this.getEditAttributes();
    this.model.saveAndPush(attrs);
    ApplicationRegistry.updateLatestUpdate(this.appName);
  },

  onDelete: function () {
    this.model.destroy();
    this.model.destroy({data: {'request_sid': REQUEST_SID}, processData: true, ajaxSync: true});
    ApplicationRegistry.updateLatestUpdate(this.appName);
  }
});

var AppModel = Backbone.Model.extend({
  /** Save the local model and push to server */
  saveAndPush: function (attrs, extraOptions) {
    this.save(); // local save
    this.save(_.extend(attrs, {request_sid: REQUEST_SID}), _.extend({ajaxSync: true}, extraOptions));
  }
});

var AppCollection = Backbone.Collection.extend({
  initialize: function () {
    this.localStorage = new Backbone.LocalStorage(this.appName + '-collection');
  },

  /** create a model locally, save it to the server and add it to the collection */
  createModel: function (model, attrs, extraOptions) {
    this.add(model, extraOptions);
    model.save(attrs, {
      ajaxSync: true,
      success: function (model, response) {
        model.save(response.instance);
      }
    });
    ApplicationRegistry.updateLatestUpdate(this.appName);
  }
});

function initComponent(componentName, componentView, componentCollection) {
  for (var i = 0; i < COMPONENTS.length; i++) {
    var component = COMPONENTS[i];
    if (component.name === componentName) {
      new componentView({
        el: '#' + component.element,
        collection: componentCollection,
        options: component.options
      });
    }
  }
}

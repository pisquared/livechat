var CSRF_TOKEN = $('meta[name=csrf-token]').attr('content');

$.ajaxSetup({
  beforeSend: function (xhr, settings) {
    if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader("X-CSRFToken", CSRF_TOKEN);
    }
  }
});

// update csrf
function updateCSRF() {
  $.ajax({
    url: '/csrf-token'
  }).done(function (csrftoken) {
    $('meta[name=csrf-token]').attr('content', CSRF_TOKEN);
    $('.csrf-token').each(function () {
      $(this).val(csrftoken);
    });
  })
}
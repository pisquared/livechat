from flask import Blueprint, current_app

client = Blueprint('client', __name__,
                   static_folder='static',
                   static_url_path='/static/client',
                   template_folder='templates')

from . import views

if current_app.config.get('DEBUG') and current_app.config.get('WEBSOCKETS_ENABLED'):
    from . import dev_websocket_views

if current_app.config.get('DEBUG') and current_app.config.get('WORKERS_ENABLED'):
    from . import dev_worker_views

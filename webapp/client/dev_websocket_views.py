from flask import request
from flask_login import current_user
from flask_socketio import join_room, emit

from shipka3.store import database
from shipka3.webapp import socketio
from shipka3.webapp.util import get_now_user


@socketio.on('SYN')
def socket_connect(json):
    payload = {'request_sid': request.sid, }
    if current_user.is_authenticated:
        tz_offset_minutes = json.get('tz_offset_minutes')
        tz_offset_seconds = -(tz_offset_minutes * 60)
        if current_user.tz_offset_seconds != tz_offset_seconds:
            current_user.tz_offset_seconds = tz_offset_seconds
            database.add(current_user)
            database.push()
        join_room(current_user.id)
        payload.update({'user': {'id': current_user.id},
                        'datetime': str(get_now_user(current_user))})
    print('< SYN: ' + str(json))
    print('> ACK: ' + str(payload))
    emit('ACK', payload)

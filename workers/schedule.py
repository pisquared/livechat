from shipka3.workers import celery

celery.conf.beat_schedule = {
    # Uncomment this to enable scheduling:
    # 'add-every-30-seconds': {
    #     'task': 'workers.tasks.sum_args',
    #     'schedule': 60.0,
    #     'args': (16, 16)
    # },
}
celery.conf.timezone = 'UTC'

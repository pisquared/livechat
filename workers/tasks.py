from shipka3.workers import celery


@celery.task
def sum_args(x, y):
    return str(x + y)

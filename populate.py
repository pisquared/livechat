def populate():
    from shipka3.store import database

    # create apps
    from shipka3.store.database import User
    admin_user = User.get_one_by(id=1)
    database.add(admin_user)
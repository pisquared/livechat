#!/usr/bin/env bash
HELP="
Usage $0 [ assets | beat | bootstrap | celery | db | dev | eventlet | flower | pgweb | populate | prod_push | recreate | reprov_project | runserver | shell | status | subapp | test ]

    assets              Manage static assets.
    beat                Run beat scheduler.
    bootstrap           Bootstrap the project by cloning shipka3 repo
    celery              Run celery worker.
    db                  Perform database migrations.
    dev                 Run default development environment - server+worker.
    eventlet            Run eventlet server.
    flower              Run flower management.
    pgweb               Run database management.
    populate            Populates a database with initial values.
    prod_push           Push to production: **!!Only if you have configured sensitive.py and have sensitive folder!!**
    recreate            Recreates everything - db, workers etc.
    reprov_project      Reprovision the project only.
    runserver           Runs the server.
        --debug-fe=True      Enable debugging of front end by not compressing of static files.
        --debug-tb=True      Enable debug toolbar.
        --ssl=True           Enable running with certificates.
        prod                 Run in production.
    shell               Runs a Python shell inside Flask application context.
    status              View the status of the installed and running services
    subapp init <name>  Initializes new subapp in the webapp directory (blueprint)
    test                Run BDD tests with lettuce

"

bootstrap() {
    [ ! -d shipka3 ] && git clone https://gitlab.com/pisquared/shipka3
    cp sensitive_config.json.sample sensitive_config.json
    VAGRANT_VERSION_EXPECTED="Vagrant 2.0.1"
    VAGRANT_VERSION=`vagrant --version`

    if [ ! "$VAGRANT_VERSION" == "$VAGRANT_VERSION_EXPECTED" ]; then
        echo "Please install $VAGRANT_VERSION_EXPECTED - https://www.vagrantup.com/downloads.html"
    fi
}

command_db() {
    DB_COMMAND=$2
    case "$DB_COMMAND" in
        migrate) ./shipka3/scripts/db_migrate.sh
        ;;
        *) ./manage.py "$@"
        ;;
    esac
    return $?
}

command_main() {
    INITIAL_COMMAND=$1
    case "$INITIAL_COMMAND" in
        assets|dev|shell|runserver|populate) ./manage.py "$@"
        ;;
        beat) ./shipka3/scripts/run_beat.sh "$@"
        ;;
        bootstrap) bootstrap
        ;;
        celery) ./shipka3/scripts/run_celery.sh "$@"
        ;;
        db) command_db "$@"
        ;;
        eventlet) ./shipka3/scripts/run_eventlet.sh "$@"
        ;;
        flower) ./shipka3/scripts/run_flower.sh "$@"
        ;;
        pgweb) ./shipka3/scripts/run_pgweb.sh "$@"
        ;;
        prod_push) ./shipka3/scripts/prod_push.sh "$@"
        ;;
        recreate) ./shipka3/scripts/recreate.sh
        ;;
        reprov_project) vagrant provision --provision-with project_specific
        ;;
        status) shift && ./shipka3/scripts/status.py "$@"
        ;;
        subapp) shift && ./shipka3/scripts/subapp.sh "$@"
        ;;
        test) lettuce
        ;;
        *) >&2 echo -e "${HELP}"
        return 1
        ;;
    esac
    return $?
}

command_main "$@"
